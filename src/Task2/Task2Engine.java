package Task2;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import AutomationFramework.SeleniumDriver;

public class Task2Engine extends SeleniumDriver {

	String baseUrl = "https://www.yandex.ru";

	public void Run() throws InterruptedException {

		CheckPhones();
		
		CheckHeadphones();
		
		_wd.quit();
	}

	private void CheckHeadphones() throws InterruptedException {

		openSection("Электроника");

		String productTitle = selectItemAndReadTitle("Наушники и Bluetooth-гарнитуры", "Samsung", "17000", "25000");

		String innerProductText = getInnerProductText();

		CheckNamesProductSame(productTitle, innerProductText);
	}

	private void CheckPhones() throws InterruptedException {

		openSection("Электроника");

		String productTitle = selectItemAndReadTitle("Мобильные телефоны", "Samsung", "40000","");

		String innerProductText = getInnerProductText();

		CheckNamesProductSame(productTitle, innerProductText);
	}

	private void openSection(String sectionName) throws InterruptedException {
		
		_wd.get(baseUrl);

		WebElement market = _wd.findElement(By.cssSelector("a[data-id='market']"));
		market.click();

		WebElement electronics = _wd.findElement(By.partialLinkText(sectionName));

		electronics.click();
	}

	private String getInnerProductText() {
		
		WebElement productElement = _wd
				.findElement(By.xpath(".//div[@class='n-product-summary__headline']/descendant::h1[1]"));

		String productText = productElement.getText();
		return productText;
	}

	private String selectItemAndReadTitle(String ItemCategory, String itemBrand, String lowPriceLimit, String upPriceLimit)
			throws InterruptedException {
		
		WebElement itemCategory = _wd.findElement(By.partialLinkText(ItemCategory));

		itemCategory.click();

		WebElement samsung = _wd.findElement(By.xpath(
				".//legend[contains(text(),'Производитель')]/following-sibling::ul/descendant::span[contains(text(),'"
						+ itemBrand + "')][1]"));

		samsung.click();

		WebElement priceFrom = _wd.findElement(By.cssSelector("input[name='Цена от']"));

		priceFrom.sendKeys(lowPriceLimit);
		
		if(!upPriceLimit.equals(""))
		{
			WebElement priceTo = _wd.findElement(By.cssSelector("input[name='Цена до']"));

			priceTo.sendKeys(upPriceLimit);
		}

		TimeUnit.SECONDS.sleep(5);

		WebElement firstItem = _wd.findElement(By.xpath(
				".//div[@class='n-snippet-list n-snippet-list_type_grid snippet-list_size_3 metrika b-zone b-spy-init i-bem metrika_js_inited snippet-list_js_inited b-spy-init_js_inited b-zone_js_inited']/child::div[1]/child::a[1]"));

		String itemTitle = firstItem.getAttribute("title");

		firstItem.click();
		
		TimeUnit.SECONDS.sleep(2);

		return itemTitle;
	}

	private void CheckNamesProductSame(String rememberedText, String productText) {

		System.out.println(rememberedText);
		System.out.println(productText);

		if (rememberedText.equals(productText)) {
			System.out.println("Names same");
		} else {
			System.out.println("Names not same");
			System.out.println(StringUtils.difference(rememberedText, productText));
		}
	}
}