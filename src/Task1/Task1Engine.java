package Task1;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class Task1Engine {
	
	public void Run(String sourceFilePath) throws IOException
	{
		Integer[] data = ReadDataFromFile(sourceFilePath);
		Integer [] dataOrderedByDec = SortData(data,true);
		
		ShowData(dataOrderedByDec);
		
		Integer [] dataOrderedByInc = SortData(data, false);
		
		ShowData(dataOrderedByInc);
	}

	public Integer[] ReadDataFromFile(String filePath) throws IOException
	{
		Path path = Paths.get(filePath);
		
		String content = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
		
		String[] strings = content.split(",");
		
		int[] numbers = Arrays.asList(strings).stream().mapToInt(Integer::parseInt).toArray();
		
		return ConvertToInteger(numbers);
	}
	
	private Integer [] ConvertToInteger(int [] source)
	{
		Integer[] convertedData = new Integer[source.length];
		int i = 0;
		for (int value : source) {
			convertedData[i++] = Integer.valueOf(value);
		}
		
		return convertedData;
	}
	
	public Integer[] SortData(Integer[] array, boolean fromMaxToMin)
	{
		Integer[] sortedArray = array;
		
		java.util.List<Integer> data = Arrays.asList(array);
		
		Arrays.sort(sortedArray);
		
		if(fromMaxToMin)
			java.util.Collections.reverse(data);			
		
		return sortedArray;
	}
	
	public void ShowData(Integer[] dataSet) {
		System.out.println("-== start show data ==-");
		for (int number : dataSet) {
			System.out.println(number);
		}
		System.out.println("-== end show data ==-");
	}
}
