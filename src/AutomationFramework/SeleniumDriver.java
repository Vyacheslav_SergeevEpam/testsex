package AutomationFramework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumDriver {

	protected WebDriver _wd;

	protected WebDriverWait _wait;	
	private int _maxTimeoutInSeconds = 10;

	public SeleniumDriver() {
		System.setProperty("webdriver.chrome.driver", "C:/eclipse/chromedriver.exe");
		_wd = new ChromeDriver();

		_wait = new WebDriverWait(_wd, _maxTimeoutInSeconds);

		_wd.get("https://www.google.com");

		_wd.manage().window().maximize();
	}

}
