package Program;
import java.io.IOException;
import Task1.Task1Engine;
import Task2.Task2Engine;
import Task3.Task3Engine;

public class Main {

	public static void main(String[] args) throws IOException, InterruptedException {
		
		Task1Engine task1Engine= new Task1Engine();
		task1Engine.Run("c:/temp/task1Data.txt");
		
		Task2Engine task2Engine= new Task2Engine();
		task2Engine.Run();
		
		Task3Engine task3Engine = new Task3Engine();
		task3Engine.Run();
	}
}
