package Task3;

import java.awt.Desktop;
import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import javax.swing.JOptionPane;
import AutomationFramework.SeleniumDriver;

public class Task3Engine extends SeleniumDriver {

	private String _baseUrl = "https://www.ya.ru";

	public void Run() throws InterruptedException, IOException {
		String tempFolderPath = java.util.UUID.randomUUID().toString();
		
		new File(tempFolderPath).mkdir();

		_wd.get(_baseUrl);

		openEpamGroupByYandex();

		openVacancies();

		openSeniorAutomationVacancy();

		saveVacancyDescriptionToFile(tempFolderPath);

		showResults(tempFolderPath);

		_wd.quit();
	}

	private void showResults(String tempFolderPath) throws IOException {
		Desktop.getDesktop().open(new File(tempFolderPath));

		JOptionPane.showMessageDialog(new Frame(), "Want you delete test directory?");

		FileUtils.deleteDirectory(new File(tempFolderPath));
	}

	private void saveVacancyDescriptionToFile(String tempFolderPath)
			throws FileNotFoundException, UnsupportedEncodingException {
		WebElement vacancyDescription = _wd
				.findElement(By.xpath(".//div[@class='recruiting-details-description-header']"));

		String vacancyDescriptionTextPart1 = vacancyDescription.getText();

		PrintWriter writer = new PrintWriter(tempFolderPath + "/vacancyDescription.txt", "UTF-8");
		writer.println(vacancyDescriptionTextPart1);

		WebElement requirementsHeader = _wd.findElement(By.xpath(".//h4[contains(text(),'Требования')]"));

		writer.println(requirementsHeader.getText());

		List<WebElement> requirementsList = _wd
				.findElements(By.xpath(".//h4[contains(text(),'Требования')]/following-sibling::ul[1]/descendant::li"));

		System.out.println(requirementsList.size());

		for (WebElement requirement : requirementsList) {
			writer.println(requirement.getText());
		}

		WebElement reward = _wd.findElement(By.xpath(".//h4[contains(text(),'Мы предлагаем')]"));

		writer.println(reward.getText());

		List<WebElement> rewardList = _wd.findElements(
				By.xpath(".//h4[contains(text(),'Мы предлагаем')]/following-sibling::ul[1]/descendant::li"));

		System.out.println(rewardList.size());

		for (WebElement rewardItem : rewardList) {
			writer.println(rewardItem.getText());
		}

		writer.close();
	}

	private void openSeniorAutomationVacancy() {
		WebElement vacancySearchInput = _wd.findElement(By.xpath(".//input[@class='job-search-input']"));

		vacancySearchInput.sendKeys("Senior Software Test Automation Engineer");

		WebElement searchBtn = _wd
				.findElement(By.xpath(".//button[@class='job-search-button location-search-submit']"));

		searchBtn.click();

		String seniorXPath = ".//ul[@class='search-result-list']/descendant::a[contains(text(), 'Senior Software Test Automation Engineer')][1]";
		
		_wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(seniorXPath)));

		WebElement automationEngineer = _wd.findElement(By.xpath(seniorXPath));

		automationEngineer.click();
	}

	private void openVacancies() {
		Actions action = new Actions(_wd);
		WebElement careers = _wd.findElement(By.xpath(".//a[@href='/careers']"));

		action.moveToElement(careers).perform();
		WebElement vacancies = _wd.findElement(By.xpath(".//a[@href='/careers/job-listings']"));

		vacancies.click();
	}

	private void openEpamGroupByYandex() {
		WebElement searchTb = _wd.findElement(By.xpath(".//input[@class='input__control input__input']"));

		searchTb.sendKeys("EPAM-Systems");

		WebElement findBtn = _wd.findElement(By.xpath(".//span[contains(text(),'Найти')]/ancestor::button[1]"));

		System.out.println(findBtn.getText());

		findBtn.click();
		
		String epamGroupXPath = ".//b[contains(text(),'epam-group.ru')]";
		
		_wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(epamGroupXPath)));

		WebElement epamGroup = _wd.findElement(By.xpath(epamGroupXPath));

		Set<String> oldWindowsSet = _wd.getWindowHandles();

		epamGroup.click();

		Set<String> newWindowsSet = _wd.getWindowHandles();

		newWindowsSet.removeAll(oldWindowsSet);
		String newWindowHandle = newWindowsSet.iterator().next();

		_wd.switchTo().window(newWindowHandle);
	}
}
